import java.util.ArrayList;
import java.util.Collections;

import java.util.stream.Collectors;

public class ConvexHull {

	//Startmetode f�r Korvexeh�lle
	public ArrayList<Point> computeHull(ArrayList<Point> arrayList) {
		// Dublikate entfernen
		arrayList = (ArrayList<Point>) arrayList.stream().distinct().collect(Collectors.toList());

		// Array aufsteigend nach x sotirren
		Collections.sort(arrayList);

		return convexHull(arrayList);
	}
	
	
	//Rekusive Methode f�r die Korvexeh�lle. Funktioniert sozusagen nach dem Mergesort Prinzip
	private ArrayList<Point> convexHull(ArrayList<Point> arrayList) {

		//Abbruch Bediung, 1 ist in sich eine Konvexeh�lle
		if (arrayList.size() == 1) {
			return arrayList;
		}
		/*
		 * TODO: Bruteforce ab 3 else if (arrayList.size() <= 3) { return
		 * bruteForceHull(arrayList); }
		 */
		
		//Punktlisten in 2 H�lften aufteilen
		int size = arrayList.size();
		ArrayList<Point> leftPoints = new ArrayList<>(arrayList.subList(0, (size) / 2));
		ArrayList<Point> riighPoints = new ArrayList<>(arrayList.subList((size) / 2, size));

		//Konvexeh�lle f�r die Tellisten erstellen
		ArrayList<Point> leftHull = convexHull(leftPoints);
		ArrayList<Point> rightHull = convexHull(riighPoints);

		//Teillisten zusammenf�gen
		return merge(leftHull, rightHull);
	}

	// TODO: Bruteforce Hull Algo for < 3
	@SuppressWarnings("unused")
	private ArrayList<Point> bruteForceHull(ArrayList<Point> arrayList) {
		return arrayList;

	}

	private ArrayList<Point> merge(ArrayList<Point> hull1, ArrayList<Point> hull2) {

		//TODO: Da funktioniert irgenwas nicht
		//Startgrade zwischen den liksten Punkt der ersten H�lle und dem rechtesten der 2 H�lle erstellen (das wird die obre Schranke)
		Point p = Collections.max(hull1);
		Point q = Collections.min(hull2);

		//Kopie machen f�r die untre Schranke
		Point cp_p = p;
		Point cp_q = q;

		//Altes p und q speichern zum vergleichen
		Point oldP = null;
		Point oldQ = null;

		// untre grade erstellen und solange a Punkten runterrotiren bis man eine obre Schranke gefunden hat
		do {
			oldP = p;
			oldQ = q;

			if (q.mCockwiseSucc != null) {
				while (new Line(p, q).side(q.mCockwiseSucc) < 0) {
					q = q.mCockwiseSucc;
				}
			}

			if (p.mCounterClockwiseSucc != null) {
				while (new Line(q, p).side(p.mCounterClockwiseSucc) > 0) {
					p = p.mCounterClockwiseSucc;
				}
			}
		} while (!(p.equals(oldP) && q.equals(oldQ)));

		oldP = null;
		oldQ = null;

		// obre Grade erstellen
		do {
			oldP = cp_p;
			oldQ = cp_q;

			if (cp_q.mCounterClockwiseSucc != null) {
				while (new Line(cp_p, cp_q).side(cp_q.mCounterClockwiseSucc) > 0) {
					cp_q = cp_q.mCounterClockwiseSucc;
				}
			}

			if (cp_p.mCounterClockwiseSucc != null) {
				while (new Line(cp_q, cp_p).side(cp_p.mCockwiseSucc) < 0) {
					cp_p = cp_p.mCockwiseSucc;
				}
			}
		} while (!(cp_p.equals(oldP) && cp_q.equals(oldQ)));
		// Liste aus der H�lle l�schen
		p.mCockwiseSucc = q;
		q.mCounterClockwiseSucc = p;

		cp_p.mCounterClockwiseSucc = cp_q;
		cp_q.mCockwiseSucc = cp_p;

		ArrayList<Point> result = new ArrayList<>();
		Point start = p;

		do {
			result.add(p);
			p = p.mCounterClockwiseSucc;
		} while (!p.equals(start));

		return result;
	}
}
