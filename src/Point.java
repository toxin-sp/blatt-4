import java.util.Arrays;

public class Point implements Comparable<Point> {

	// Array f�r die Kordinaten
	private final double[] mKordinaten;
	public final double mX, mY;

	public Point mCockwiseSucc = null;
	public Point mCounterClockwiseSucc = null;

	// Konstruktor zu Inizalsirung
	public Point(final double... kordinaten) {
		assert kordinaten.length != 1 : "Es erscheint sinnlos ein Punkt in einer Dimension zu haben";

		if (kordinaten.length == 0) {
			throw new IllegalArgumentException("Es kann keinen Punkt ohne Kordinaten geben");
		}
		this.mKordinaten = kordinaten;

		mX = mKordinaten[0];
		mY = mKordinaten[1];
	}

	public double getI(int i) {
		if (i >= this.mKordinaten.length) {
			throw new ArrayIndexOutOfBoundsException("Es gibt keine entsprechenede Kordinate im Objekt");
		} else if (i < 0) {
			throw new ArrayIndexOutOfBoundsException("Es kann keine negativen Kordinaten im Objekt geben");
		} else {
			return this.mKordinaten[i];
		}
	}

	//Die Anzahl der Dimensionen zur�ckgeben
	public int getR() {
		return this.mKordinaten.length;
	}

	@Override
	public boolean equals(Object obj) {
		// Wenn das Objekt mit sich selbstverglichen wird
		if (obj == this) {
			return true;
		}
		// Wenn das Objekt nicht vom Typen Points ist...
		if (!(obj instanceof Point)) {
			return false;
		}

		final Point pointToCompare = (Point) obj;

		// Wenn das Objekt eine andre Dimension hat
		if (pointToCompare.getR() != this.getR()) {
			return false;
		}

		// Die einzlen Elmente vergleichen
		for (int i = 0; i < this.mKordinaten.length; i++) {
			if (this.mKordinaten[i] != pointToCompare.getI(i)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return Arrays.toString(this.mKordinaten);
	}

	//Den Punkt kopiren ohne Verk�pfung
	public Point copy() {
		return new Point(this.mKordinaten);
	}

	//CompareTo Metohode um absteidgend zu sotireren
	@Override
	public int compareTo(Point p) {
		if (p == this)
			return 0;
		return this.mX > p.mX ? +1 : this.mX < p.mX ? -1 : 0;
	}
}
