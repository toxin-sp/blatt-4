import java.util.ArrayList;
import java.util.Random;

public class PointsGenerator {
	private final double mMin, mMax;
	private final Random mRand;

	public PointsGenerator(final double min, final double max) {
		assert min != max : "Es erscheint nicht sinvoll das minimumn aufs Maximus zu setzen";

		if (min > max) {
			throw new IllegalArgumentException("Das Minimum draf nicht gr��e sein als das Maximum");
		}
		this.mMin = min;
		this.mMax = max;
		mRand = new Random();
	}

	//Liste mit zuf�lligen Punkten zwischen min und max erstellen mit der Anzahl n
	ArrayList<Point> generate(int n) {
		assert n != 0 : "Es ist nicht sinvoll eine leere Liste anzufodern";

		//Parameter auf Fehler �berprfen
		if (n < 0) {
			throw new IllegalArgumentException("Es kann keine negative Anzahl an Punkten generiert werden");
		}

		//liste anlegen und zahlen erzeugen
		ArrayList<Point> list = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			Point point = new Point(this.mMin + (this.mMax - this.mMin) * mRand.nextDouble(),
					this.mMin + (this.mMax - this.mMin) * mRand.nextDouble());
			
			list.add(point);
		}
		return list;
	}
}
