public class Line {

	//Konstante falls er nochmal d Dimensioen geben soll
	private final boolean MULTIDIMENSION = false;
	//Die beiden Punkte
	private final Point mPoint1, mPoint2;

	//Grade anlgen und Kordinaten speichern mit fehlerbehandlung
	public Line(Point p1, Point p2) {
		if (p1 == null || p2 == null) {
			throw new IllegalArgumentException("Es darf kein Punkt mit Null �bergeben werden");
		}
		if (p1.getR() != p2.getR()) {
			throw new IllegalArgumentException("Es kann keine Grade zwischen Punkten verschidner Dimensioen geben");
		}
		if (p1.equals(p2)) {
			throw new IllegalArgumentException("Es kann keine Grade zwischen dem gleichen Punkt erstellt werden"); 
		}
		if (!MULTIDIMENSION && p1.getR() != 2) {
			throw new IllegalArgumentException("Es sind nut Punkte mit 2 Dimension erlaubt");
		}

		this.mPoint1 = p1;
		this.mPoint2 = p2;
	}

	//Gibt zur�ck ob der Punkt links rechts oder auf der Graden lieget  
	public int side(Point p1) {
		if (p1 == null) {
			throw new IllegalArgumentException("Der Punkt darf nicht null sein");
		}
		if (p1.getR() != 2) {
			throw new IllegalArgumentException("Es gibt keine Deintion von rechts und links au�erhabl des R^2");
		}
		return determineSideOfPoint(p1);
	}

	//Hilfsmethode die mit dem Kreuntzprodunkt der derei Punkte die Seite bestimmt auf der der Punkt p1 liegt
	private int determineSideOfPoint(Point p1) {
		double r = ((this.mPoint2.getI(0) - this.mPoint1.getI(0)) * (p1.getI(1) - this.mPoint1.getI(1))
				- (this.mPoint2.getI(1) - this.mPoint1.getI(1)) * (p1.getI(0) - this.mPoint1.getI(0)));

		if (r == 0)
			return 0; // Auf der Line
		else if (r > 0)
			return 1; // Links von der Line
		else
			/* (r < 0) */ return -1; // Rechts von der Line
	}

}