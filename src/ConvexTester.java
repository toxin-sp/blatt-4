import java.util.ArrayList;
import java.util.stream.Collectors;

public class ConvexTester {

	// 3 Kolineare Punkte p,q,r
	// Pr�fen ob der r Punkt zwischen 'pq' auf der Stecke liegt
	static boolean istDerPunktAufDerStrecke(Point p, Point r, Point q) {
		return (r.mX <= Math.max(p.mX, q.mX) && r.mX >= Math.min(p.mX, q.mX) && r.mY <= Math.max(p.mY, q.mY)
				&& r.mY >= Math.min(p.mY, q.mY));
	}

	//ausrichtung aus 3 Punkten ermitteln als sozusagen ob die Punkte auf einer Line liegen oder im/gegen den Uhrzeigersinn
	static int ausrichtung(Point p, Point q, Point r) {
		double val = ((q.mY - p.mY) * (r.mX - q.mX) - (q.mX - p.mX) * (r.mY - q.mY));

		if (val == 0) {
			return 0; // colinear
		}
		return (val > 0) ? 1 : 2; // im Uhrzeigersinn oder gegen den Uhrzeigersinn
	}

	// gibt wahr zr�ck wenn sich die Strecken p1q1 und p2q2 schneiden
	static boolean schneidenZweiStrecken(Point p1, Point q1, Point p2, Point q2) {
		//Die Ausrichtung der 3 Punkte zur Fallbehaldung finden
		int ausrichtung1 = ausrichtung(p1, q1, p2);
		int ausrichtung2 = ausrichtung(p1, q1, q2);
		int ausrichtung3 = ausrichtung(p2, q2, p1);
		int ausrichtung4 = ausrichtung(p2, q2, q1);

		// Normalerfall (Punkt ist in der H�lle)
		if (ausrichtung1 != ausrichtung2 && ausrichtung3 != ausrichtung4) {
			return true;
		}

		// Sonderf�lle:
		// p1, q1, p2 sind colinear
		// p2 liegt auf der Strecke p1q1
		if (ausrichtung1 == 0 && istDerPunktAufDerStrecke(p1, p2, q1)) 
			return true;
		
		// p1, q1, p2 sind colinear
		// q2 liegt auf der Strecke p1q1
		if (ausrichtung2 == 0 && istDerPunktAufDerStrecke(p1, q2, q1)) 
			return true;
		
		// p2, q2 and p1 sind colinear
		// p1 liegt auf der Strecke p2q2
		if (ausrichtung3 == 0 && istDerPunktAufDerStrecke(p2, p1, q2)) 
			return true;
		
		// p2, q2 q1 sind colinear
		// q1 liegt  auf der Streke p2q2
		if (ausrichtung4 == 0 && istDerPunktAufDerStrecke(p2, q1, q2)) 
			return true;
		
		//Die Strecken scheiden sich nicht
		return false;
	}

	//Gibt wahr zr�ck falls der Punkt in der H�lle lie
	static boolean istPunktImPolygon(ArrayList<Point> hull, int n, Point p) {
		// Es k�nnen keine Punkte in weniger als 3 Punkten liegen au�er sie sind colinear
		if (n < 3) {
			return false;
		}

		//Horizontale Strecke erstellen auf der H�he vom Punkt
		Point extreme = new Point(Double.MAX_VALUE - 200, p.mY);

		//Z�hlen wie oft sich die Line mit der polygon der Convexenh�lle schneident
		int count = 0, i = 0;
		do {
			int next = (i + 1) % n;

			//Wenn die horzonatle Strecke von p mit einer Strecke der H�lle schneident
			if (schneidenZweiStrecken(hull.get(i), hull.get(next), p, extreme)) {
				// Wenn der Punkt p is colinear mit der Strecke iNext ist, dann pr�fen ob er auf dem Streckenabschnitt liegt
				if (ausrichtung(hull.get(i), p, hull.get(next)) == 0) {
					return istDerPunktAufDerStrecke(hull.get(i), p, hull.get(next));
				}

				count++;
			}
			i = next;
		} while (i != 0);

		// wahr zur�ckgeben falls die Zahl ungrade ist, denn dann ist der Punkt in der H�lle
		return (count % 2 == 1);
	}

	//Methode zur festellung ob alle Punkte in der Corvexenh�lle leigen
	public static int isConvexHull(ArrayList<Point> arrayList, ArrayList<Point> hull) {
		//weniger als 3 Punkte bilden zwarngsweise eine H�lle au�er sie liegn auf einer Line
		if (arrayList.size() < 3) {
			return 0;
		}
		
		arrayList = (ArrayList<Point>) arrayList.stream().distinct().collect(Collectors.toList());

		int n = 0;
		for (Point p : arrayList) {
			if (!istPunktImPolygon(hull, hull.size(), p)) {
				System.out.println("NOT:" + p);
				n++;
			}
		}
		return n;
	}

	public static void main(String[] args) {

		//Variabeln f�r die Benutzereingaben  
		double min = 0;
		double max = 0;
		int anzahl = 0;

		
		//Fehlerbehandlung der Benutzer Parameter
		if (args.length == 3) {
			try {
				min = Double.parseDouble(args[0]);
				max = Double.parseDouble(args[1]);
				anzahl = Integer.parseInt(args[2]);
			} catch (NumberFormatException e) {
				System.out.println("Fehler: Einer der 3  Paramter sind keine Nummern");
				System.exit(-1);
			}
		} else {
			System.out.println("FEHLER: Es m�ssen 3 Paramter angegeeben werden");
			System.exit(-1);
		}

		if (anzahl <= 0) {
			System.out.println(anzahl == 0 ? "FEHLER: Es darf nicht 0 Punkte geben"
					: "FEHLER: Es darf keine negative Anzahl an Punkten angebene werden.");
			System.exit(-1);
		}
		if (min > max) {
			System.out.println("FEHLER: Das Minimum darf nicht gr��er sein als das Maximum");
		}
	

		//Zuf�llige Liste mit Punkten anlegen
		ArrayList<Point> pointsForConvexHullList = new PointsGenerator(min, max).generate(anzahl);
		
		//Corvexe H�lle berechen und in Liste ablegen
		ArrayList<Point> convexHullResultList = new ConvexHull().computeHull(pointsForConvexHullList);

		
		//Punkte ausgeben
		System.out.println(anzahl);
		for (Point p : pointsForConvexHullList) {
			System.out.println(p);
		}
		//Anzahl der Punkte ausgebem
		System.out.println(convexHullResultList.size());
		//Punkte der Corvexen H�lle ausgeben
		for (Point p : convexHullResultList) {
			System.out.println(p);
		}

		//Anzahl der Punkte bestimmen die nicht in der H�lle liegen
		final int n = isConvexHull(pointsForConvexHullList, convexHullResultList);
		//Ausgabe der punkte die nicht in der H�lle liegen oder alle ist gut
		System.out.println(n == 0 ? "Alle Punkte sind in der Huelle!" : "Es sind " + n + " Punkte nicht in der H�lle");
	}

}
